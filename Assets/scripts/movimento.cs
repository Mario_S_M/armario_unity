﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimento : MonoBehaviour
{
    public GameObject cube;
    Touch initTouch;
    bool swiped = false;

    // Update is called once per frame
    void Update()
    {
        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Began)
            {
                initTouch = t;
            }
            else if (t.phase == TouchPhase.Moved && !swiped)
            {
                float xMoved = initTouch.position.x - t.position.x;
                float yMoved = initTouch.position.y - t.position.y;
                float distance = Mathf.Sqrt((xMoved * xMoved) + (yMoved * yMoved)); // h2 = p2 + b2
                bool swipedLeft = Mathf.Abs(xMoved) > Mathf.Abs(yMoved);

                if (distance > 50f)
                {
                    if (swipedLeft && xMoved > 0) // swiped left
                    {
                        cube.transform.Translate(-5, 0, 0);
                    }
                    else if (swipedLeft && xMoved < 0) // swped right
                    {
                        cube.transform.Translate(5, 0, 0);
                    }
                    else if (swipedLeft == false && yMoved > 0) // swiped up
                    {
                        cube.transform.Translate(0, -5, 0);
                    }
                    else if (swipedLeft == false && yMoved < 0) // swiped down
                    {
                        cube.transform.Translate(0, 5, 0);
                    }
                    swiped = true;
                }
            }
            else if (t.phase == TouchPhase.Ended)
            {
                initTouch = new Touch();
                swiped = false;
            }
        }
    }
}


