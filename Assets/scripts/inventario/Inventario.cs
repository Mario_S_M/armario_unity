﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventario : MonoBehaviour
{
 
    private float inventory_width, inventory_height;
    public int slots;
    public int rows;
    public float slots_padding_left, slots_padding_top;
    public float slot_size;

    public GameObject slot_prefab;
    private RectTransform inventory_rect;
    private List<GameObject> allslots;

    //trabalha na parte de abrir e fechar o inventario
    private bool fading_in;
    private bool fading_out;
    public float fade_time;

    private static CanvasGroup canvas_group;    

    public static CanvasGroup p_canvas_group
    {
        get
        {
            return canvas_group;
        }
    }

    void Start ()
    {

        canvas_group = transform.parent.GetComponent<CanvasGroup>();
        create_layout();
    }
	
	
	void Update ()
    {

        //abre inventario
		if(Input.GetKeyDown(KeyCode.H))
        {
            if(canvas_group.alpha >0)
            {
                StartCoroutine("fade_out");
            }
            else
            {
                StartCoroutine("fade_in");
            }
        }
        //trabalha dentro do inventario      
	}

    private void create_layout()
    {
        //---------------------------------------------------------------------------------------------------//
        // calculando o tamanho width = (slots/rows)*(slot_size + slots_padding_left) + slots_padding_left
        // calculando o tamanho weight = rows*(slot_size + slots_padding_top) + slots_padding_top
        //---------------------------------------------------------------------------------------------------//
        allslots = new List<GameObject>();

        inventory_width = (slots / rows) * (slot_size + slots_padding_left) + slots_padding_left;
        inventory_height = rows * (slot_size + slots_padding_top) + slots_padding_top;

        inventory_rect = GetComponent<RectTransform>();

        inventory_rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, inventory_width);
        inventory_rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, inventory_height);

        int columns = slots / rows;

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                GameObject new_slots = (GameObject)Instantiate(slot_prefab);

                RectTransform slot_rect = new_slots.GetComponent<RectTransform>();
                new_slots.name = "slot";

                new_slots.transform.SetParent(this.transform.parent);

                slot_rect.localPosition = inventory_rect.localPosition + new Vector3(slots_padding_left * (x + 1) + (slot_size * x), -slots_padding_top * (y + 1)-(slot_size*y));

                slot_rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slot_size);
                slot_rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slot_size);

                allslots.Add(new_slots);

            }
        }
    }

    public void move_item (GameObject clicked)
    {
      
    }

    private IEnumerator fade_out()
    {
        if(!fading_out)
        {
            fading_out = true;
            fading_in = false;
            StopCoroutine("fade_in");

            float start_alpha = canvas_group.alpha;
            float rate = 1.0f / fade_time;

            float progress = 0.0f;
            
            while(progress < 1.0f)
            {
                canvas_group.alpha = Mathf.Lerp(start_alpha,0,progress);
                progress += rate * Time.deltaTime;

                yield return null;
            }
            canvas_group.alpha = 0;
            fading_out = false;
        }
    }
    private IEnumerator fade_in()
    {
        if (!fading_in)
        {
            fading_in = true;
            fading_out = false;

            StopCoroutine("fade_out");

            float start_alpha = canvas_group.alpha;
            float progress = 0.0f;

            float rate = 1.0f / fade_time;

            while (progress < 1.0f)
            {
                canvas_group.alpha = Mathf.Lerp(start_alpha, 1, progress);
                progress += rate * Time.deltaTime;

                yield return null;
            }
            canvas_group.alpha = 1;
            fading_in = false;
        }
    }
}
