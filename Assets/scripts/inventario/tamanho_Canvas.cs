﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class tamanho_Canvas : MonoBehaviour
{

    private CanvasScaler scaler;

	void Start ()
    {
        // atualiza o tamanho do UI conforme o tamanho da tela.
        scaler = GetComponent<CanvasScaler>();
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
	}
	

}
