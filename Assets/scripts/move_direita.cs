﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_direita : MonoBehaviour
{
    Rigidbody colisao;
    public float distance = 10f;
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
            transform.Translate(Vector3.left * Time.deltaTime * distance);
        if (Input.GetMouseButtonDown(0))
            transform.Translate(Vector3.right * Time.deltaTime * distance);
    }

}
